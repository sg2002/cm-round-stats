
const sqlite3 = require('sqlite3').verbose();
const readFile = require('fs').readFile;
const express = require('express');
const async = require('async');

const dbName = 'cm.sqlite';
// const db = new sqlite3.Database(dbName);
const db = require('knex')({
    client: 'sqlite3',
    connection: {
	filename: dbName
    },
    useNullAsDefault: true
});

async function maintainSchema() {
    const exists = await db.schema.hasTable('rounds').then((exists) => exists);
    if(!exists){
	await db.schema.createTable('rounds', table => {
	    table.increments('id').primary();
	    table.date('date_start');
	    table.date('date_end');
	    table.string('map');
	    table.string('outcome');
	});
    }
}

maintainSchema();

const command = process.argv[2];
const argv = require('yargs').argv;

switch(command){
case 'import':
    import_json(argv.f);
    break;
case 'visualize':
    visualize(argv.t)
    break;
default:
    visualize(argv.t)
    break;
}

function import_json(file){
    readFile(file, (err, dataBuffer) => {
	const value = JSON.parse(dataBuffer.toString());
	import_rounds(value.messages);
    })
}

function import_rounds(messages){
    const new_round_msg = '@New Round Alert A new round is starting! The map is ';
    const round_end_msg= 'Round has ended. ';
    var rounds = [];
    var round = {};
    while(messages.length > 0){
	message = messages.shift();
	if(message.content.match(round_end_msg)){
	    round.outcome = message.content.replace(round_end_msg, '');
	    if(round.outcome.slice(-1) == '.'){
		round.outcome = round.outcome.slice(0, -1);
	    }
	    round.date_end = message.timestamp;
	}else if (message.content.match(new_round_msg)){
	    round.date_end = message.timestamp;
	}
	if(round.map){
	    if(round.date_end &&
	       (new Date(round.date_end).getTime() -
		new Date(round.date_start).getTime())/60000 > 540){
		round.date_end = null;
		round.outcome = null;
	    }
	    rounds.push(round);
	    round = {};
	}
	if (message.content.match(new_round_msg)){
	    round.map = message.content.replace(new_round_msg, '');
	    if(round.map.slice(-1) == '!'){
		round.map = round.map.slice(0, -1);
	    }
	    if(round.map == 'Corsat'){
		round.map = 'CORSAT';
	    }else if(round.map == 'Big Red'){
		round.map = 'Solaris Ridge';
	    }else if(round.map == 'Sorokyne'){
		round.map = 'Sorokyne Strata';
	    }else if(round.map == 'Prison Station'){
		round.map = 'Fiorina Cellblocks';
	    }
	    round.date_start = message.timestamp;
	}
    }
    if(round.map && !round.outcome){
	rounds.push(round);
    }
    db.batchInsert('rounds', rounds, 100)
	.then(function(ids){
	    console.log('Inserted.');
	    process.exit();
	})
	.catch(function(error){
	    console.log(error);
	    process.exit();
	});
}

function outcome_graph(res, date_from, date_to, map, simplify, display){
    const date_from_string = date_from.toISOString().split('T')[0];
    const date_to_string = date_to.toISOString().split('T')[0];
    const hours = [12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
    var base_query = db('rounds').whereNotNull('outcome').andWhere('date_start', '>=', date_from_string).andWhere('date_start', "<", date_to_string);
    if(map){
	base_query.andWhere('map', '=', map);
    }
    async.parallel(
	[
	    function(cb){
		db('rounds').select('map').distinct().whereNotNull('map').orderBy('map').pluck('map').asCallback(cb);
	    },
	    function(cb){
		base_query.clone().first('date_start').andWhere('outcome', '!=', 'Draw').orderBy('date_start').asCallback(cb);
	    },
	    function(cb){
		base_query.clone().first('date_start').andWhere('outcome', '!=', 'Draw').orderBy('date_start', 'desc').asCallback(cb);
	    },
	    function(callback){
		async.map(
		    hours,
		    (h, cb) => {
			base_query.clone().select(db.raw('outcome, count(*) as c'))
			    .andWhere(db.raw('CAST(strftime(\'%H\', date_start) AS integer) = ?', h)).groupBy('outcome').asCallback(cb);
		    }, callback)
	    }],
	(err, results) => {
	    var mmajor = [];
	    var mminor = [];
	    var xmajor = [];
	    var xminor = [];
	    var draw = [];
	    for(i=0;i<24;i++){
		var rmmajor = null;
		var rmminor = null;
		var rxmajor = null;
		var rxminor = null;
		var rdraw = null;
		results[3][i].forEach(outcome => {
		    if (outcome.outcome == 'Marine Major Victory'){
			rmmajor = outcome.c;
		    }else if (outcome.outcome == 'Marine Minor Victory'){
			rmminor = outcome.c;
		    }else if (outcome.outcome == 'Xeno Major Victory'){
			rxmajor = outcome.c;
		    }else if (outcome.outcome == 'Xeno Minor Victory'){
			rxminor = outcome.c;
		    }else if (outcome.outcome == 'Draw'){
			rdraw = outcome.c;
		    }
		});
		total = rmmajor + rmminor + rxmajor + rxminor + rdraw;
		if(display=='percentage'){
		    mmajor.push(rmmajor / total * 100);
		    mminor.push(rmminor / total * 100);
		    xmajor.push(rxmajor / total * 100);
		    xminor.push(rxminor / total * 100);
		    draw.push(rdraw / total * 100);
		}else{
		    mmajor.push(rmmajor);
		    mminor.push(rmminor);
		    xmajor.push(rxmajor);
		    xminor.push(rxminor);
		    draw.push(rdraw);
		}
	    }
	    if(simplify == 0){
		var data = [{x:hours, y:mmajor, type: 'line', name:'Marine Major'},
			    {x:hours, y:mminor, type: 'line', name:'Marine Minor',
			     line:{color: 'rgb(200, 66, 245)'}},
			    {x:hours, y:xmajor, type: 'line', name:'Xeno Major',
			     line:{color: 'rgb(52, 179, 58)'}},
			    {x:hours, y:xminor, type: 'line', name:'Xeno Minor',
			     line:{color: 'rgb(252, 186, 3)'}},
			    {x:hours, y:draw, type: 'line', name:'Draw',
			   line:{color: 'rgb(255, 0, 0)'}}];
	    }else{
		const marine = mmajor.map((e, i) => e+mminor[i]);
		const xeno = xmajor.map((e, i) => e+xminor[i]);
		var data = [{x:hours, y:marine, type: 'line', name:'Marine'},
			    {x:hours, y:xeno, type: 'line', name:'Xeno',
			     line:{color: 'rgb(52, 179, 58)'}}];
	    }
	    var layout = {
		xaxis: {
		    type: 'category'
		},
		height: 500,
		width: 1000,
		autosize: false
	    };
	    layout.title = 'CM round outcome ';
	    if(display == 'percentage'){
		layout.title += 'percentage ';
		layout.yaxis = {
		    fixedrange: true,
		    range: [0, 80]
		};
	    }
	    layout.title += 'per UTC roundstart hour ';
	    if(map){
		layout.title += 'on '+map+' ';
	    }
	    layout.title += 'from '+
		(typeof results[1] !== 'undefined' ? results[1]['date_start'].split('T')[0] : date_from_string)+' to '+
		(typeof results[2] !== 'undefined' ? results[2]['date_start'].split('T')[0] : date_to_string);
	    var maps = results[0];
	    res.render('index',
		       {type: 'outcome',
			data: data,
			map: map,
			maps: maps,
			from: date_from_string,
			to: date_to_string,
			simplify: simplify,
			display: display,
			layout: layout});
	});
}

function duration_graph(res, date_from, date_to, map, simplify, display){
    const date_from_string = date_from.toISOString().split('T')[0];
    const date_to_string = date_to.toISOString().split('T')[0];
    const hours = [12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
    var base_query = db('rounds').whereNotNull('outcome').andWhere('date_start', '>=', date_from_string).andWhere('date_start', "<", date_to_string);
    if(map){
	base_query.andWhere('map', '=', map);
    }
    async.parallel(
	[
    	    function(cb){
		db('rounds').select('map').distinct().whereNotNull('map').orderBy('map').pluck('map').asCallback(cb);
	    },
	    function(cb){
		base_query.clone().first('date_start').andWhere('outcome', '!=', 'Draw').orderBy('date_start').asCallback(cb);
	    },
	    function(cb){
		base_query.clone().first('date_start').andWhere('outcome', '!=', 'Draw').orderBy('date_start', 'desc').asCallback(cb);
	    },
	    function(cb){
		async.map(
		    hours,
		    (h, callback) => {
			base_query.clone().select(
			    db.raw('AVG(CAST((JulianDay(date_end) - JulianDay(date_start)) * 24 * 60 As integer)) as duration'))
			    .andWhere(db.raw('CAST(strftime(\'%H\', date_start) AS integer) = ?', h)).asCallback(callback);
		    }, cb);
	    },
	    function(cb){
		async.map(
		    hours,
		    (h, callback) => {
			base_query.clone().select(
			    db.raw('outcome, AVG(CAST((JulianDay(date_end) - JulianDay(date_start)) * 24 * 60 As integer)) as duration'))
			    .andWhere(db.raw('CAST(strftime(\'%H\', date_start) AS integer) = ?', h))
			    .groupBy('outcome').asCallback(callback);
		    }, cb);
	    }],
	(err, results) => {
	    var mmajor = [];
	    var mminor = [];
	    var xmajor = [];
	    var xminor = [];
	    var draw = [];
	    var all = [];
	    for(i=0;i<24;i++){
		var rmmajor = null;
		var rmminor = null;
		var rxmajor = null;
		var rxminor = null;
		var rdraw = null;
		var rall = results[3][i][0].duration ? results[3][i][0].duration : 0;
		all.push(rall);
		results[4][i].forEach(r => {
		    if (r.outcome == 'Marine Major Victory'){
			rmmajor = r.duration;
		    }else if (r.outcome == 'Marine Minor Victory'){
			rmminor = r.duration;
		    }else if (r.outcome == 'Xeno Major Victory'){
			rxmajor = r.duration;
		    }else if (r.outcome == 'Xeno Minor Victory'){
			rxminor = r.duration;
		    }else if (r.outcome == 'Draw'){
			rdraw = r.duration;
		    }
		});
		mmajor.push(rmmajor);
		mminor.push(rmminor);
		xmajor.push(rxmajor);
		xminor.push(rxminor);
		draw.push(rdraw);
	    }
	    const data = [{x:hours, y:mmajor, type: 'line', name:'Marine Major'},
			  {x:hours, y:mminor, type: 'line', name:'Marine Minor',
			   line:{color: 'rgb(200, 66, 245)'}},
			  {x:hours, y:xmajor, type: 'line', name:'Xeno Major',
			   line:{color: 'rgb(52, 179, 58)'}},
			  {x:hours, y:xminor, type: 'line', name:'Xeno Minor',
			   line:{color: 'rgb(252, 186, 3)'}},
			  {x:hours, y:draw, type: 'line', name:'Draw',
			   line:{color: 'rgb(255, 0, 0)'}},
			  {x:hours, y:all, type: 'line', name:'All Outcomes',
			   line:{color: 'rgb(0, 0, 0)'}}];
	    var layout = {
		xaxis: {
		    type: 'category'
		},
		yaxis: {
		    fixedrange: true,
		    range: [0, 190]
		},
		height: 600,
		width: 1000,
		autosize: false
	    };
	    layout.title = 'CM round duration ';
	    layout.title += 'per UTC roundstart hour ';
	    if(map){
		layout.title += 'on '+map+' ';
	    }
	    layout.title += 'from '+
		(typeof results[1] !== 'undefined' ? results[1]['date_start'].split('T')[0] : date_from_string)+' to '+
		(typeof results[2] !== 'undefined' ? results[2]['date_start'].split('T')[0] : date_to_string);
	    var maps = results[0];
	    res.render('index',
		       {type: 'duration',
			data: data,
			map: map,
			maps: maps,
			from: date_from_string,
			to: date_to_string,
			simplify: simplify,
			display: display,
			layout: layout});
	});
}

function visualize(type){
    const app = express();
    app.set('view engine', 'ejs');
    app.get('/', (req, res) => {
	if(req.query.type){
	    var type = req.query.type;
	}else{
	    var type = 'outcome';
	}
	if(req.query.from){
	    var date_from = new Date(req.query.from);
	}else{
	    var date_from = new Date();
	    date_from.setMonth(date_from.getMonth() - 3);
	}
	if(req.query.to){
	    var date_to = new Date(req.query.to);
	}else{
	    var date_to = new Date();
	}
	if(req.query.map){
	    var map = req.query.map;
	}else{
	    var map = null;
	}
	if(req.query.simplify){
	    var simplify = req.query.simplify;
	}else{
	    var simplify = 0;
	}
	if(req.query.display){
	    var display = req.query.display;
	}else{
	    var display = 'percentage';
	}
	if(type == 'duration'){
	    duration_graph(res, date_from, date_to, map, simplify, display);
	}else{
	    outcome_graph(res, date_from, date_to, map, simplify, display);
	}
    });
    app.listen(3000);
}
