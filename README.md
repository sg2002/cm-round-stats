# cm-round-stats
This is a small visualizer utility that processes [CM-SS13](https://cm-ss13.com/forums/) round data and creates graphs from it.
## Getting data
You need to export #new-round-alert channel message history from CM Discord using [DiscordChatExporter](https://github.com/Tyrrrz/DiscordChatExporter). Save it in json format. Note that using DiscordChatExporter is technically against Discord TOS.
## Setup
1. Make sure you have [Node.js](https://nodejs.org/en/) installed.
2. Navigate to the repository folder and do `npm install` in it.
3. Import a CM discord #new-round-alert channel message history dump using `node index import -f nra.json`
4. Do `node index` and navigate to [localhost:3000](http://localhost:3000/)